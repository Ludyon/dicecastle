__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"


class Castle:
    def __init__(self):
        self._quest_path = (1, 2, 3, 4, 5,
                            6, 7, 8, 9, 10,
                            11, 12, 13, 14,
                            15, 16, "Master Sword")

    @property
    def quest_path(self):
        return self._quest_path
