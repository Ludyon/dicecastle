__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

from dice import Dice
from player import Player


class Event:
    dice = Dice()
    player = Player()

    def __init__(self):
        self._case_monster_event = [2, 5, 15, 16]

    @property
    def case_monster_event(self):
        return self._case_monster_event

    def event_initialize(self, player_position):
        if player_position == 2 or player_position == 5:
            monster_throw = self.dice.throw()
            if 1 <= monster_throw <= 2:
                self.player.life -= 1
                print(f"You face a Monster Level 1! The Monster rolls the dice {monster_throw}, you lose 1 hp! ")
                print(f"Your life : {self.player.life}")
            else:
                print("You met a Monster Level 1, by chance, you leave victorious of this fight")
        elif player_position == 15:
            monster_throw = self.dice.throw()
            if 1 <= monster_throw <= 3:
                self.player.life -= 1
                print(f"You face a Monster Level 2! The Monster rolls the dice {monster_throw}, you lose 1 hp! ")
                print(f"Your life : {self.player.life}")
            else:
                print("You met a Monster Level 2, by chance, you leave victorious of this fight")
        elif player_position == 16:
            monster_throw = self.dice.throw()
            if 1 <= monster_throw <= 3:
                self.player.life -= 2
                print(f"You face a Monster Level 3! The Monster rolls the dice {monster_throw}, you lose 2 hp! ")
                print(f"Your life : {self.player.life}")
            else:
                print("You met a Monster Level 3, by chance, you leave victorious of this fight")
        elif player_position == 13:
            self.player.take("Potion x1")
            self.player.take("Potion x1")


class Monster:
    def __init__(self, level):
        self._level = f"Monster Level {level}"


monster_level_1 = Monster(1)
monster_level_2 = Monster(2)


class LifePotion(Event):
    pass
