__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import unittest

from src.dice import Dice


class TestDice(unittest.TestCase):
    """ Test Dice """

    def test_dice_initial_value(self):
        # GIVEN
        dice = Dice()

        # WHEN
        initial_value = dice._dice

        # THEN
        self.assertEqual(initial_value, 0)

    def test_dice_throw(self):
        # GIVEN
        dice = Dice()

        # WHEN
        throw = dice.throw()

        # THEN
        self.assertTrue(1 <= throw <= 6)


if __name__ == '__main__':
    unittest.main()
