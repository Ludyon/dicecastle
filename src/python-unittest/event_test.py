__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import unittest

from src.event import Event


class TestEvent(unittest.TestCase):
    """ Test Event"""

    def test_event_size_case_monster_event(self):
        # GIVEN
        event = Event()

        # WHEN
        size = len(event.case_monster_event)

        # THEN
        self.assertEqual(size, 4)

    def test_event_case_monster_event(self):
        # GIVEN
        event = Event()

        # WHEN
        case_monster_event = event.case_monster_event

        # THEN
        self.assertListEqual(case_monster_event, [2, 5, 15, 16])


if __name__ == '__main__':
    unittest.main()
