__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import unittest

from src.castle import Castle


class TestCastle(unittest.TestCase):
    """ Test Castle """

    def test_castle_size(self):
        # GIVEN
        castle = Castle()

        # WHEN
        size = len(castle._quest_path)

        # THEN
        self.assertEqual(size, 17)

    def test_castle_quest_path(self):
        # GIVEN
        castle = Castle()

        # WHEN
        quest_path = castle.quest_path

        # THEN
        self.assertTupleEqual(quest_path, (1, 2, 3, 4, 5,
                                           6, 7, 8, 9, 10,
                                           11, 12, 13, 14,
                                           15, 16, "Master Sword"))

    def test_castle_last_position(self):
        # GIVEN
        castle = Castle()

        # WHEN
        last_position = castle._quest_path[-1]

        # THEN
        self.assertEqual(last_position, "Master Sword")


if __name__ == '__main__':
    unittest.main()
