import unittest

from ..hero import Hero
from ..game import Game



class test_dicecastle(unittest.TestCase):

#Le héros démarre la partie à l'extérieur du chateau.

    def test_player_must_start_outdoor(self):

        #GIVEN
        hero = Hero()
        #WHEN
        test = hero._position
        #THEN
        self.assertEqual(test, 0)

#A chaque tour, le héros avance d'une case.

    def test_player_must_move_1(self):

        #GIVEN
        hero = Hero()
        #WHEN
        hero.move()
        #THEN
        self.assertEqual(hero._position, 1)

# Le chateau est constitué d'un enchainement de 17 cases.
    
    def test_player_get_sword(self):
        #GIVEN
        hero = Hero()
        game = Game(hero)
        #WHEN        
        for i in range (0,17):
            game.turn()
        #THEN
        self.assertEqual(hero._position, 17)




        
