__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import unittest

from src.game import Game


class TestGame(unittest.TestCase):
    """ Test Game"""

    def test_game_castle_size(self):
        # GIVEN
        game = Game()
        
        # WHEN
        size = game.castle_size()
        
        # THEN
        self.assertEqual(size, 17)


if __name__ == '__main__':
    unittest.main()
