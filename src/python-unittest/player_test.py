__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import unittest

from src.player import Player


class TestPlayer(unittest.TestCase):
    """ Test Player """

    """ Position """

    def test_player_must_start_outdoor(self):
        # GIVEN
        player = Player()

        # WHEN
        position = player.position

        # THEN
        self.assertEqual(position, 0)

    """ Life """

    def test_player_life(self):
        # GIVEN
        player = Player()

        # WHEN
        life = player.life

        # THEN
        self.assertEqual(life, 3)

    def test_player_min_life(self):
        # GIVEN
        player = Player()

        # WHEN
        min_life = player.min_life

        # THEN
        self.assertEqual(min_life, 1)

    def test_player_max_life(self):
        # GIVEN
        player = Player()

        # WHEN
        max_life = player.max_life

        # THEN
        self.assertEqual(max_life, 5)

    def test_player_dead(self):
        # GIVEN
        player = Player()

        # WHEN
        dead = player.dead

        # THEN
        self.assertEqual(dead, 0)

    """ Throw"""

    def test_player_must_throw(self):
        # GIVEN
        player = Player()

        # WHEN
        throw = player.throw()

        # THEN
        self.assertTrue(1 <= throw <= 6)

    """ Move """

    def test_player_must_move(self):
        # GIVEN
        player = Player()

        # WHEN
        move = player.move()

        # THEN
        self.assertEqual(move, player.position)


if __name__ == '__main__':
    unittest.main()
