__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

from dice import Dice


class Player:
    def __init__(self):
        self._position = 0
        self._life = 3
        self._min_life = 1
        self._max_life = 5
        self._dead = 0
        self._inventory = []

    """ Position """

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = value

    """ Life """

    @property
    def life(self):
        return self._life

    @life.setter
    def life(self, value):
        self._life = value

    @property
    def min_life(self):
        return self._min_life

    @property
    def max_life(self):
        return self._max_life

    @property
    def dead(self):
        return self._dead

    @dead.setter
    def dead(self, value):
        self._dead = value

    def life_care(self):
        self._life += 2

    """ Throw """

    @staticmethod
    def throw():
        dice = Dice()
        return dice.throw()

    """ Move """

    def move(self):
        self._position += self.throw()
        return self._position

    """ Inventory """

    def inventory(self):
        for item in self._inventory:
            print(item)

    def take(self, new_item):
        self._inventory.append(new_item)
