__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

from castle import Castle
from dice import Dice
from event import Event
from player import Player


class Game:
    castle = Castle()
    dice = Dice()
    player = Player()
    event = Event()

    """ Game """

    START = """
        \t Long ago, Ganon, the Prince of Darkness, stole the Triforce of Strength.
        \t He used its power to attack the kingdom of Hyrule, the world of light, and took control of it."
        \t Then he sent eight monsters to the land to strengthen his dominance.
        """

    SHOW = f"""
        \tYour life : {player.life}
        \tYour inventory : {player.inventory()}
        """

    print(START)
    print(SHOW)

    while player.position < len(castle.quest_path):
        if player.life <= player.dead:
            print("You are dead! GAME OVER")
            break
        elif player.life > player.dead:
            want_to_play = input("Want to play? ")
            if player.position < len(castle.quest_path):
                if want_to_play != "no":
                    player.position = player.move()
                    event.event_initialize(player.position)
                else:
                    break
                if player.position >= len(castle.quest_path):
                    player.position = len(castle.quest_path)
                    if 1<= player.throw() <= 2:
                        player.take("Master Sword")
                        print("You get the Master Sword! It allows you to defeat Ganon and GFTO of the castle.")
                    print(f"You are currently on the {player.position} square of the castle!")
                    print("You didn't get the sword back, you are a coward. You take your legs to your neck.")
                    break
                print(f"You are currently on the {player.position} square of the castle!")
    
    print(f"""
          \tYou are a nice player! You can try to succeed again!
          \t
          \tYour life : {player.life}
          \tYour inventory : {player.inventory()}
          \t
          \tTHE END
          """)


play = Game()
