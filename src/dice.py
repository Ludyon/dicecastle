__author__ = "MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 MAZET Michael, LAVIGNE Lucas, BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

import random


class Dice:
    def __init__(self):
        self._dice = 0

    @staticmethod
    def throw():
        return random.randint(1, 6)
